Categories:Office,Internet
License:GPLv3
Web Site:https://www.linuxcounter.net/
Source Code:https://github.com/alexloehner/linuxcounter.android.app
Issue Tracker:https://github.com/alexloehner/linuxcounter.android.app/issues

Name:LinuxCounter
Auto Name:lico-update
Summary:Companion for LinuxCounter project
Description:
Companion app for the [https://en.wikipedia.org/wiki/Linux_Counter LinuxCounter]
project (LiCo).
.

Repo Type:git
Repo:https://github.com/alexloehner/linuxcounter.android.app

Build:0.0.5,5
    commit=2e9f3ab6fcd649b1fd1bccca8503e6bd252efecd
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/volley/d' build.gradle && sed -i -e '/support-v4/acompile "com.mcxiaoke.volley:library:1.0.+@aar"' build.gradle

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.0.5
Current Version Code:5

